from math import *
import pygame
import time


class Tacka:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def Euklidska_udaljenost(t1, t2: Tacka):
    return sqrt(pow(t1.x - t2.x, 2) + pow(t1.y - t2.y, 2))


class kovanica:
    def __init__(self, poluprecnik, centar: Tacka):
        self.poluprecnik = poluprecnik
        self.centar = centar


class stol:
    def __init__(self, poluprecnik,centar: Tacka):
        self.poluprecnik = poluprecnik
        self.centar = centar


def da_li_se_sijeku(kovanica1, kovanica2: kovanica):
    udaljenost = Euklidska_udaljenost(kovanica1.centar, kovanica2.centar)
    if udaljenost >= (kovanica1.poluprecnik + kovanica2.poluprecnik):
        return False
    else:
        return True


def provjera_presjeka(kovanica1, kovanice:[kovanica]):

    for i in kovanice:
        if da_li_se_sijeku(i, kovanica1):
            return True
    return False


def da_li_je_unutra(kovanica1: kovanica, stol1: stol):
    udaljenost = Euklidska_udaljenost(kovanica1.centar, stol1.centar) + kovanica1.poluprecnik
    if udaljenost >= stol1.poluprecnik:
        return False
    else:
        return True


def igra(stol1: stol,poluprecnik):

    kovanice = []

    pygame.init()
    screen = pygame.display.set_mode((800, 600))

    pygame.draw.circle(screen, (255, 0, 0), (stol1.centar.x, stol1.centar.y), stol1.poluprecnik, 0)
    pygame.display.flip()
    time.sleep(2)
    pygame.draw.circle(screen, (0, 0, 255), (stol1.centar.x, stol1.centar.y), poluprecnik, 0)
    kovanice.append(kovanica(poluprecnik, Tacka(stol1.centar.x,stol1.centar.y)))
    pygame.display.update()
    brojac = 0
    while True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit();
                quit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()

                pot_kovanica = kovanica(poluprecnik, Tacka(x, y))
                if provjera_presjeka(pot_kovanica, kovanice) == False and da_li_je_unutra(pot_kovanica, stol1) == True:

                    kovanice.append(pot_kovanica)
                    pygame.draw.circle(screen, (0, 255, 0), (pot_kovanica.centar.x, pot_kovanica.centar.y), pot_kovanica.poluprecnik, 0)
                    pygame.display.update()
                    auto_kovanica = kovanica(poluprecnik, Tacka(2 * stol1.centar.x - x, 2 * stol1.centar.y - y))
                    kovanice.append(auto_kovanica)
                    time.sleep(1)
                    pygame.draw.circle(screen, (0, 0, 255), (auto_kovanica.centar.x, auto_kovanica.centar.y),
                                       auto_kovanica.poluprecnik, 0)
                    pygame.display.update()
                    brojac = 0
                else:
                    brojac+=1

                if brojac == 3:
                    font = pygame.font.SysFont("comicsansms", 72)

                    text = font.render("You lose", True, (0, 128, 0))
                    screen.blit(text,
                                (300, 300))
                    pygame.display.update()
                    time.sleep(2)
                    pygame.quit()




if __name__ == '__main__':
    stol1 = stol(250, Tacka(380, 260))
    igra(stol1, 35)
